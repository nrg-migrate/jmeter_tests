#!/bin/sh
HOST=$1
PORT=$2
SCRIPT=$3
USER=$4
PASS=$5
PROJ_PREFIX=$6
ABBR=$7
RUN="${8}000"
PROJ_PREFIX="JMETER_"
NUM_SUBJECTS=20
NUM_SCANS=1
NUM_FILES=1
USERS=$9
LOGS=${10}

function process {
	label=$1
	args=$2
	rm logs/${3}.log
	rm logs/${3}.jtl
	rm $LOGS/${3}.jtl
	echo -e "/n Starting ($label) with -j logs/${3}.log -l logs/${3}.jtl -n $args -Jhost=$HOST -Jport=$PORT -Jscript=$SCRIPT -Jusername=$USER -Jpassword=$PASS -Jproj_prefix=$PROJ_PREFIX -Jmin_subject=1$RUN -Jsubjects=$NUM_SUBJECTS -Jnum_scans=$NUM_SCANS -Jmin_scans=1 -Jmax_scans=$NUM_SCANS -Jnum_files=$NUM_FILES -Jmin_file=1 -Jmax_file=$NUM_FILES"
	../bin/jmeter.sh -j logs/${3}.log -l logs/${3}.jtl -n $args -Jhost=$HOST -Jport=$PORT -Jscript=$SCRIPT -Jusername=$USER -Jpassword=$PASS -Jproj_prefix=$PROJ_PREFIX -Jmin_subject=1$RUN -Jsubjects=$NUM_SUBJECTS -Jnum_scans=$NUM_SCANS -Jmin_scans=1 -Jmax_scans=$NUM_SCANS -Jnum_files=$NUM_FILES -Jmin_file=1 -Jmax_file=$NUM_FILES
	grep "summary =" logs/${3}.log | tail -1 | sed "s/summary =/$label:/g" | sed "s/INFO.*Summariser//g" >> jmeter_results_$ABBR.txt
	tail -n1 jmeter_results_$ABBR.txt
	cat logs/${3}.jtl >> $LOGS/combined.jtl
}
rm $LOGS/combined.jtl

process "creating projects" "-t ./CreateProjects.jmx -Jusers=1 -Jramp=10 -Jprojects=$USERS " "projects"

process "creating subjects x 50" "-t ./CreateSubjects.jmx -Jusers=$USERS -Jramp=10 " "subjects"

process "creating sessions x 50" "-t ./CreateSessions.jmx -Jusers=$USERS -Jramp=10 " "sessions"

process "creating scans x 50" "-t ./CreateScans.jmx -Jusers=$USERS -Jramp=10 " "scans"

process "creating resources x 50" "-t ./CreateResources.jmx -Jusers=$USERS -Jramp=10 " "resources"

process "creating files x 50" "-t ./UploadFiles.jmx -Jusers=$USERS -Jramp=10 " "upload"

process "download files x 50" "-t ./DownloadFiles.jmx -Jusers=$USERS -Jramp=10 " "download"

process "download zips x 50" "-t ./DownloadZip.jmx -Jusers=$USERS -Jramp=10 " "download_zips"
